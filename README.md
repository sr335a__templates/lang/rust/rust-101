# Getting Started with Rust 

### Installing Rust 

> Ensure you have admin privilege

```Cmd
choco install rust
```

> Once the installation is complete, you can verify by checking the `cargo` version
>
> ```cmd
> system32 $ cargo -V
> cargo 1.58.0 (f01b232bc 2022-01-19)
> ```

> You can also check the versions of `rustc` 
>
> ```cmd
> Rust $ rustup -V
> rustup 1.21.1 (7832b2ebe 2019-12-20)
> Rust $ rustc -V
> rustc 1.58.1 (db9d1b20b 2022-01-20)
> Rust $ 
> ```



### Hello World

*File naming convention*

Create a folder to hold your project; let's call it `hello_world`

```cmd
mkdir hello_world
cd hello_world
```

The Rust files are identified by the extension of `*.rs` 

Let's create a `main.rs` and place the following code! 

If there are more words in the file name the preferred naming convention is to separate the words with `_`

> The Hello-World Code (`main.rs`)
>
> ```rust
> fn main() {
> 	println!("Hello World!");
> }
> ```

##### Compiling and running 

> To execute the hello-world code we need to compile and create the executable.
>
> ```cmd
> rustc main.rs
> ```
>
> The above command compiles the main.rs into binary executable. Once the executable is generated we can execute the same.
>
> ```cmd
> hello_worl $ .\main.exe
> Hello World!
> ```
>
> In your linux env
>
> ```sh
> sanker@DESKTOP-2KI0183:~/rs-projects/HW$ ls -lsh
> total 4.5M
> 4.5M -rwxr-xr-x 1 sanker sanker 2.5M Jan 29 19:37 main
>    0 -rw-r--r-- 1 sanker sanker   48 Jan 29 19:37 main.rs
> sanker@DESKTOP-2KI0183:~/rs-projects/HW$ ./main
> Hello Friends! GiG!
> sanker@DESKTOP-2KI0183:~/rs-projects/HW$
> ```



### Anatomy of our Hello World

> We have written the main function using`fn main()` with no parameters and no return. Within the `main` function we are calling a macro `println!`. 
>
> The `main` function is the main entry point for the program.
>
> We used the `rustc` to compile the program and create the executable that can be run standalone and shared without a rust installed. 



### Creating and managing your Rust Projects using `Cargo`

> Cargo is Rust’s build system and package manager. Most Rustaceans use this tool to manage their Rust projects because Cargo handles a lot of tasks for you, such as building your code, downloading the libraries your code depends on, and building those libraries. (We call the libraries that your code needs *dependencies*.)
>
> Ensure your env has cargo using the following command 
>
> ```cmd 
> cargo -V
> ```



##### Creating your first project using `Cargo`

> Use the following `new` command of `cargo` to create your project
>
> ```cmd 
> cargo new hello_cargo
> cd hello_cargo
> ```
>
> The above command creates a `binary-project` with the following structure and content.
>
> ```cmd
> Created binary (application) `hello_world` package
> ```
>
> ![image-20220129200532482](README.assets/image-20220129200532482.png)

##### Building your project

> TOML is used for the configuration language by cargo, the `Cargo.toml` file contains the project details and the dependencies information required by Cargo to build the project.
>
> - Cargo `check` is used to check the project for the compilation readiness and the executables are not generated.
> - Cargo `build` compiles the source after pulling the dependencies and generates compiled output.  
>
> ```cmd
> cargo check
> cargo build
> ```
>
> The executable gets generated under the `target/debug`
>
> ![image-20220129212332951](README.assets/image-20220129212332951.png)
>
> 
>
> **Building a release**
>
> For building a release need to use the `--release` qualifier for the build command and the release would be ready under the `target/release`location
>
> ```cmd
> cargo build --release
> ```
>
> ![image-20220129212712589](README.assets/image-20220129212712589.png)










